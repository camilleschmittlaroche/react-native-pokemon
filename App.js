//React
import React, {Fragment} from 'react';

//Redux
import {Provider} from 'react-redux';
import {NativeRouter, Route} from 'react-router-native';
import configureStore from './src/app/store/configureStore';

//Components
import PokemonPageContainer from './src/app/containers/PokemonPageContainer';
import PokemonsPageContainer from './src/app/containers/PokemonsPageContainer';

const store = configureStore();

const App = () => (
    <Provider store={store}>
        <NativeRouter>
            <Fragment>
                <Route exact path="/" component={PokemonsPageContainer} />
                <Route exact path="/pokemon/:pokemonId" component={PokemonPageContainer} />
            </Fragment>
        </NativeRouter>
    </Provider>
);

export default App;
