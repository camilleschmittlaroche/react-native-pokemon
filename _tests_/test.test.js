import {firstLetterToUpperCase, initPokemon, initPokemons} from '../src/app/actions/pokemon_actions';
import reducer from '../src/app/reducer/pokemon_reducer';
import {
    CREATE_POKEMON,
    DELETE_POKEMON, GET_ALL_POKEMONS,
    GET_POKEMON,
    POKEMON_FAILURE,
    UPDATE_POKEMON,
} from '../src/app/action-types/pokemon_types';

describe('test initial state', () => {
    const initState = {
        pokemons: {},
        pokemon: {},
        isFetching: false,
        errors: {},
    };

    it('firstLetterToUpperCase', () => {
        const minuscule = 'poke';
        expect(firstLetterToUpperCase(minuscule)).toEqual('Poke');
    });
    it('Majuscule 2', () => {
        const minuscule = 'POKE';
        expect(firstLetterToUpperCase(minuscule)).toEqual('Poke');
    });
    it('Majuscule 2', () => {
        const minuscule = 'poke Eygviu';
        expect(firstLetterToUpperCase(minuscule)).toEqual('Poke eygviu');
    });

    it('Majuscule null', () => {
        expect(firstLetterToUpperCase(null)).toEqual(null);
    });

    it('initial state', () => {
        const action = {
            type: 'INIT_POKEMON_STATE',
        };
        expect(reducer(undefined, action)).toEqual(initState);
    });

    it('is fetching', () => {
        const action = {
            type: 'POKEMON_REQUEST',
        };
        expect(reducer(initState, action)).toEqual({
            pokemons: {},
            pokemon: {},
            isFetching: true,
            errors: {},
        });
    });

    it('failure', () => {
        const action = {
            type: POKEMON_FAILURE,
            payload: {
                toto: 'forbidden',
            },
        };
        expect(reducer(initState, action)).toEqual({
            pokemons: {},
            pokemon: {},
            isFetching: false,
            errors: {
                toto: 'forbidden',
            },
        });
    });

    it('create', () => {
        const action = {
            type: CREATE_POKEMON,
            payload: {
                _id: 100,
                name: 'Pike',
                url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
            },
        };
        expect(reducer(initState, action)).toEqual({
            pokemons: {
                100: {
                    _id: 100,
                    name: 'Pike',
                    url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
                },
            },
            pokemon: {
                _id: 100,
                name: 'Pike',
                url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
            },
            isFetching: false,
            errors: {},
        });
    });

    it('update', () => {
        const action = {
            type: UPDATE_POKEMON,
            payload: {
                _id: 100,
                name: 'Pike',
                url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
            },
        };
        expect(reducer(initState, action)).toEqual({
            pokemons: {
                100: {
                    _id: 100,
                    name: 'Pike',
                    url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
                },
            },
            pokemon: {
                _id: 100,
                name: 'Pike',
                url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
            },
            isFetching: false,
            errors: {},
        });
    });

    it('delete', () => {
        const action = {
            type: DELETE_POKEMON,
            payload: 100,
        };
        const state = {
            pokemons: {
                100: {
                    _id: 100,
                    name: 'Pike',
                    url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
                },
            },
            pokemon: {},
            isFetching: false,
            errors: {},
        };
        expect(reducer(state, action)).toEqual({
            pokemons: {},
            pokemon: {},
            isFetching: false,
            errors: {},
        });
    });

    it('get one', () => {
        const action = {
            type: GET_POKEMON,
            payload: {
                _id: 100,
                name: 'Pike',
                url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
            },
        };
        const state = {
            pokemons: {},
            pokemon: {
                _id: 100,
                name: 'Pike',
                url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
            },
            isFetching: false,
            errors: {},
        };
        expect(reducer(state, action)).toEqual({
            pokemons: {
                100: {
                    _id: 100,
                    name: 'Pike',
                    url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
                },
            },
            pokemon: {
                _id: 100,
                name: 'Pike',
                url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
            },
            isFetching: false,
            errors: {},
        });
    });

    it('get all', () => {
        const action = {
            type: GET_ALL_POKEMONS,
            payload: {
                100: {
                    _id: 100,
                    name: 'Pike',
                    url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
                },
                101: {
                    _id: 101,
                    name: 'Pika',
                    url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
                },
            },
        };
        const state = {
            pokemons: {},
            pokemon: {
                _id: 100,
                name: 'Pike',
                url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
            },
            isFetching: false,
            errors: {},
        };
        expect(reducer(state, action)).toEqual({
            pokemons: {
                100: {
                    _id: 100,
                    name: 'Pike',
                    url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
                },
                101: {
                    _id: 101,
                    name: 'Pika',
                    url: 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg',
                },
            },
            pokemon: {},
            isFetching: false,
            errors: {},
        });
    });

    it('InitPokemon', () => {
        const pokemon = {
            id: 10,
            name: 'pika',
            sprites: {
                front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/132.png',
            },
            types: [
                {
                    slot: 1,
                    type: {
                        name: 'normal',
                        url: 'https://pokeapi.co/api/v2/type/1/',
                    },
                },
            ],
        };
        expect(initPokemon(pokemon)).toEqual({
            _id: 10,
            name: 'Pika',
            avatar: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/132.png',
            types: ['normal'],
        });
    });

    it('InitPokemons', () => {
        const pokemons = [
            {
                id: 10,
                name: 'pika',
            },
            {
                id: 11,
                name: 'piko',
            },
        ];

        expect(initPokemons(pokemons)).toEqual({
            1: {
                _id: 1,
                name: 'Pika',
            },
            2: {
                _id: 2,
                name: 'Piko',
            },
        });
    });
});
