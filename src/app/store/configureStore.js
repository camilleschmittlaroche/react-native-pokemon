// Redux
import {createStore, applyMiddleware, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';

// Reducer
import rootReducer from './rootReducer';

let composeEnhancers = '';

if(process.env.NODE_ENV === 'production') {
    composeEnhancers = compose;
} else if(window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
} else {
    composeEnhancers = compose;
}

const enhancer = composeEnhancers(applyMiddleware(thunkMiddleware));

export default function () {
    return createStore(
        rootReducer,
        undefined,
        enhancer,
    );
}
