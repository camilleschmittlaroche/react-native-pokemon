import {combineReducers} from 'redux';
import pokemonReducer from '../reducer/pokemon_reducer';

export default combineReducers({
    pokemonReducer,
});
