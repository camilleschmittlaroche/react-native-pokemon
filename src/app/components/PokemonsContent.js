//React
import React, {Component, Fragment} from 'react';

//React-native
import {ActivityIndicator, FlatList, Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

//Lodash
import values from 'lodash/values';

import PropTypes from 'prop-types';


const styles = StyleSheet.create({
    container: {
        marginTop: 20,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 15,
        marginBottom: 5,
        backgroundColor: 'skyblue',
    },
    id: {
        marginTop: 15,
        fontSize: 20,
    },
    name: {
        fontSize: 18,
        textAlign: 'center',
        margin: 15,
        color: '#5159ee',
        fontWeight: 'bold',
    },
});

class PokemonsContent extends Component {
    renderItem = ({item}) => {
        const defaultAvatar = 'https://res.cloudinary.com/aa1997/image/upload/v1535930682/pokeball-image.jpg';
        const name = item.name.charAt(0).toUpperCase() + item.name.slice(1);

        return (
            <TouchableOpacity onPress={() => this.props.onPress(item._id)}>
                <View style={styles.row}>
                    <Text style={styles.id}>
                        {item._id}
                    </Text>
                    <Text style={styles.name}>
                        {name}
                    </Text>
                    <Image
                        style={{width: 50, height: 50}}
                        source={{uri: item.avatar ? item.avatar : defaultAvatar}}
                    />
                </View>
            </TouchableOpacity>
        );
    };

    renderContent = () => {
        if(this.props.isFetching) {
            return (
                <ActivityIndicator />
            );
        }
        const arrayPokemons = values(this.props.pokemons);
        return (
            <FlatList
                contentContainerStyle={styles.container}
                data={arrayPokemons}
                keyExtractor={item => item._id.toString()}
                renderItem={this.renderItem}
            />
        );
    };

    render() {
        return (
            <Fragment>
                {this.renderContent()}
            </Fragment>
        );
    }
}

export default PokemonsContent;

PokemonsContent.propTypes = {
    isFetching: PropTypes.bool.isRequired,
    pokemons: PropTypes.array.isRequired,
};
