//React
import React, {Fragment, Component} from 'react';

//React-native
import {Image, StyleSheet, Text, View} from 'react-native';

//Actions
import PropTypes from 'prop-types';
import {firstLetterToUpperCase} from '../actions/pokemon_actions';

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        marginBottom: 20,
        flex: 1,
    },

    avatarContainerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    avatarStyle: {
        width: 200,
        height: 200,
    },

    dataContainerStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'skyblue',
    },

    idStyle: {
        fontSize: 20,
        justifyContent: 'flex-start',
        color: '#5159ee',
    },

    nameStyle: {
        fontSize: 24,
        textAlign: 'center',
        justifyContent: 'center',
        color: '#5159ee',
        alignItems: 'center',
    },

    textTypeStyle: {
        marginLeft: 15,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 20,
        color: 'white',

    },
});

const getTypeColor = (type) => {
    switch(type) {
        case 'fire':
            return '#f08030';

        case 'normal':
            return '#a8a878';

        case 'fighting':
            return '#c03028';

        case 'flying':
            return '#a890f0';

        case 'water':
            return '#6890f0';

        case 'grass':
            return '#78c850';

        case 'poison':
            return '#a04aA0';

        case 'electric':
            return '#f8d030';

        case 'ground':
            return '#e0c068';

        case 'psychic':
            return '#f85888';

        case 'rock':
            return '#b8a038';

        case 'ice':
            return '#98d8d8';

        case 'bug':
            return '#a8b820';

        case 'dragon':
            return '#7038f8';

        case 'ghost':
            return '#705898';

        case 'dark':
            return '#705848';

        case 'steel':
            return '#b8b8d0';

        case 'fairy':
            return '#ee99ac';

        default:
            return '#68a090';
    }
};

const getStyles = (type) => ({
        backgroundColor: getTypeColor(type),
        height: 50,
        width: 100,
        borderRadius: 50,
    });

class PokemonContent extends Component {
    renderType = (type) => (
        <View key={type} style={getStyles(type)}>
            <Text style={styles.textTypeStyle}>
                {firstLetterToUpperCase(type)}
            </Text>
        </View>
    );

    render() {
        return (
            <View style={styles.container}>

                <View style={styles.avatarContainerStyle}>
                    <Image
                        style={styles.avatarStyle}
                        source={{uri: this.props.pokemon.avatar}}
                        resizeMode="cover"
                    />

                </View>

                <View style={styles.dataContainerStyle}>
                    <Text style={styles.idStyle}>
                        { this.props.pokemon._id}
                    </Text>
                    <Text style={styles.nameStyle}>
                        { this.props.pokemon.name}
                    </Text>
                </View>

                <View style={styles.typeContainerStyle}>
                    <Fragment>
                        {
                            this.props.pokemon.types !== undefined ?
                                this.props.pokemon.types.map(this.renderType) : null }
                    </Fragment>
                </View>

            </View>
        );
    }
}

PokemonContent.propTypes = {
    _id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    types: PropTypes.array.isRequired,
    avatar: PropTypes.string.isRequired,
};

export default PokemonContent;
