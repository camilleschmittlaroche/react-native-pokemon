// React
import React, {Fragment} from 'react';

//React-native
import {ActivityIndicator} from 'react-native';

//Component
import PropTypes from 'prop-types';
import PokemonsContentContainer from '../containers/PokemonsContentContainer';

const PokemonsPage = ({pokemons, isFetching}) => (
    <Fragment>
        {
            isFetching ?
                <ActivityIndicator /> :
                <PokemonsContentContainer pokemons={pokemons} />
        }
    </Fragment>
);

PokemonsPage.propTypes = {
    pokemons: PropTypes.array.isRequired,
    isFetching: PropTypes.bool.isRequired,
};

export default PokemonsPage;
