// React
import React, {Component, Fragment} from 'react';

//React-native
import {ActivityIndicator} from 'react-native';

import PropTypes from 'prop-types';

//Component
import PokemonContentContainer from '../containers/PokemonContentContainer';

const PokemonPage = ({isFetching}) => (
        <Fragment>
            {
                isFetching ?
                    <ActivityIndicator /> :
                    <PokemonContentContainer />
            }
        </Fragment>
);

PokemonPage.propTypes = {
    isFetching: PropTypes.bool.isRequired,
};

export default PokemonPage;
