//Lodash
import omit from 'lodash/omit';

// Actions_type
import {
    INIT_POKEMON_STATE,

    POKEMON_REQUEST,
    POKEMON_FAILURE,

    GET_ALL_POKEMONS,

    CREATE_POKEMON,
    GET_POKEMON,
    UPDATE_POKEMON,
    DELETE_POKEMON,
} from '../action-types/pokemon_types';


const initialState = {
    pokemons: {},
    pokemon: {},
    isFetching: false,
    errors: {},
};

const reducer = (state = initialState, action) => {
    let newPokemons: *;
    console.log(action.payload);
    switch(action.type) {
        case INIT_POKEMON_STATE:
            return initialState;

        case POKEMON_REQUEST:
            return {
                ...state,
                isFetching: true,
            };

        case POKEMON_FAILURE:
            return {
                ...state,
                isFetching: false,
                errors: action.payload,
            };

        case GET_ALL_POKEMONS:
            return {
                ...state,
                pokemons: action.payload,
                pokemon: {},
                isFetching: false,
                errors: {},
            };

        case CREATE_POKEMON:
            return {
                ...state,
                pokemons: {
                    ...state.pokemons,
                    [action.payload._id]: action.payload,
                },
                pokemon: action.payload,
                isFetching: false,
                errors: {},
            };

        case GET_POKEMON:
            return {
                ...state,
                pokemons: {
                    ...state.pokemons,
                    [action.payload._id]: action.payload,
                },
                pokemon: action.payload,
                isFetching: false,
                errors: {},
            };

        case UPDATE_POKEMON:
            return {
                ...state,
                pokemons: {
                    ...state.pokemons,
                    [action.payload._id]: action.payload,
                },
                pokemon: action.payload,
                isFetching: false,
                errors: {},
            };

        case DELETE_POKEMON:
            newPokemons = omit(state.pokemons, action.payload);
            return {
                ...state,
                pokemons: newPokemons,
                pokemon: {},
                isFetching: false,
                errors: {},
            };

        default:
            return state;
    }
};

export default reducer;
