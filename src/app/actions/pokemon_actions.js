// Axios
import axios from 'axios/index';

// Action_types
import {
    POKEMON_REQUEST,
    POKEMON_FAILURE,

    GET_ALL_POKEMONS,

    CREATE_POKEMON,
    GET_POKEMON,
    UPDATE_POKEMON,
    DELETE_POKEMON,
} from '../action-types/pokemon_types';

function pokemonRequest() {
    return {
        type: POKEMON_REQUEST,
    };
}

function pokemonFailure(errors) {
    return {
        type: POKEMON_FAILURE,
        payload: errors,
    };
}

function getAllPokemons(pokemons) {
    return {
        type: GET_ALL_POKEMONS,
        payload: pokemons,
    };
}

function createPokemon(pokemon) {
    return {
        type: CREATE_POKEMON,
        payload: pokemon,
    };
}

function getPokemon(pokemon) {
    return {
        type: GET_POKEMON,
        payload: pokemon,
    };
}

function updatePokemon(pokemon) {
    return {
        type: UPDATE_POKEMON,
        payload: pokemon,
    };
}

function deletePokemon(_id) {
    return {
        type: DELETE_POKEMON,
        payload: _id,
    };
}

export function firstLetterToUpperCase(str) {
    if(!str) {
        return null;
    }

    return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}

export function initPokemons(pokemons) {
    let objectPokemons = {};

    pokemons.map((pokemon, index) => {
        const objectPokemon = {
            _id: index + 1,
            name: firstLetterToUpperCase(pokemon.name),
        };

        objectPokemons = {
            ...objectPokemons,
            [objectPokemon._id]: objectPokemon,
        };
    });

    return objectPokemons;
}

export function initPokemon(pokemon) {
    let arrayTypes = [];
    for(let i = 0; i < pokemon.types.length; i++) {
        arrayTypes.push(pokemon.types[i].type.name);
    }
    return {
        _id: pokemon.id,
        name: firstLetterToUpperCase(pokemon.name),
        avatar: pokemon.sprites.front_default,
        types: arrayTypes,
    };
}

export function fetchAllPokemons() {
    return dispatch => {
        dispatch(pokemonRequest());

        const url = 'https://pokeapi.co/api/v2/pokemon?limit=964';

        return axios.get(url)
            .then(response => {
                const pokemons = response.data.results;
                const payload = initPokemons(pokemons);

                dispatch(getAllPokemons(payload));
            })
            .catch(error => {
                dispatch(pokemonFailure(error.response));
            });
    };
}

export function fetchPokemon(pokemonId) {
    const url = 'https://pokeapi.co/api/v2/pokemon/' + pokemonId;
    return dispatch => {
        dispatch(pokemonRequest());
        return axios.get(url)
            .then(response => {
                const pokemon = response.data;
                const payload = initPokemon(pokemon);

                dispatch(getPokemon(payload));
            })
            .catch(error => {
                dispatch(pokemonFailure(error.response));
            });
    };
}

export function fetchCreatePokemon(teamId, body) {
    return dispatch => {
        dispatch(pokemonRequest());

        const url = 'https://pokeapi.co/api/v2/pokemon/' + teamId;

        return axios.post(url, body)
            .then(response => {
                const pokemon = response.data.data;

                dispatch(createPokemon(pokemon));
            })
            .catch(error => {
                dispatch(pokemonFailure(error.response));
            });
    };
}

export function fetchUpdatePokemon(adId, body) {
    return dispatch => {
        dispatch(pokemonRequest());

        const url = 'https://pokeapi.co/api/v2/pokemon/' + adId;

        return axios.post(url, body)
            .then(response => {
                const pokemon = response.data;

                dispatch(updatePokemon(pokemon));
            })
            .catch(error => {
                dispatch(updatePokemon(error.response));
            });
    };
}

export function fetchDeletePokemon(adId, body) {
    return dispatch => {
        dispatch(pokemonRequest());
        const url = 'https://pokeapi.co/api/v2/pokemon/' + adId;

        return axios.post(url, body)
            .then(() => {
                dispatch(deletePokemon(adId));
            })
            .catch(error => {
                dispatch(deletePokemon(error.response));
            });
    };
}
