//React
import React, {Component} from 'react';

//Redux
import {connect} from 'react-redux';

//Component
import PropTypes from 'prop-types';
import PokemonPage from '../components/PokemonPage';

// Actions
import {fetchPokemon} from '../actions/pokemon_actions';

class PokemonPageContainer extends Component {
    componentDidMount() {
        this.props.fetchPokemon(this.props.match.params.pokemonId);
    }

    render() {
        return (
            <PokemonPage isFetching={this.props.isFetching} />

        );
    }
}

const mapStateToProps = state => ({
    isFetching: state.pokemonReducer.isFetching,
});

PokemonPageContainer.propTypes = {
    isFetching: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, {fetchPokemon})(PokemonPageContainer);
