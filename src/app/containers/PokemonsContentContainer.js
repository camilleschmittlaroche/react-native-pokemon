// React
import React, {Component} from 'react';
import PropTypes from 'prop-types';

//React Router
import {withRouter} from 'react-router-dom';

// Redux
import {connect} from 'react-redux';
import {fetchAllPokemons} from '../actions/pokemon_actions';

// Component
import PokemonsContent from '../components/PokemonsContent';

class PokemonsContentContainer extends Component {
    onPress = (pokemonId) => {
        const url = '/pokemon/' + pokemonId;
        this.props.history.push(url);
    };
    
    render() {
        return (
            <PokemonsContent onPress={this.onPress} pokemons={this.props.pokemons} />
        );
    }
}

const mapStateToProps = state => ({
    pokemons: state.pokemonReducer.pokemons,
});

PokemonsContentContainer.propTypes = {
    pokemons: PropTypes.array.isRequired,
};

const connectedComponent = connect(mapStateToProps, {fetchAllPokemons})(PokemonsContentContainer);
export default withRouter(connectedComponent);
