// React
import React, {Component} from 'react';

// Redux
import {connect} from 'react-redux';

// Lodash
import values from 'lodash/values';
import isEmpty from 'lodash/isEmpty';

// Component
import PropTypes from 'prop-types';
import PokemonsPage from '../components/PokemonsPage';

// Actions
import {fetchAllPokemons} from '../actions/pokemon_actions';

class PokemonsPageContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pokemons: [],
        };
    }

    componentDidMount() {
        if(isEmpty(this.props.pokemons)) {
            this.props.fetchAllPokemons();
        }
    }

    componentWillReceiveProps(nextProps) {
        const arrayPokemons = values(nextProps.pokemons);
        this.setState(
            {
                pokemons: arrayPokemons.slice(),
            },
        );
    }

    render() {
        return (
            <PokemonsPage isFetching={this.props.isFetching} pokemons={this.state.pokemons} />
        );
    }
}

const mapStateToProps = state => ({
    pokemons: state.pokemonReducer.pokemons,
    isFetching: state.pokemonReducer.isFetching,
    errors: state.pokemonReducer.errors,
});

PokemonsPageContainer.propTypes = {
    isFetching: PropTypes.bool.isRequired,
    pokemons: PropTypes.array.isRequired,
};

export default connect(mapStateToProps, {fetchAllPokemons})(PokemonsPageContainer);
