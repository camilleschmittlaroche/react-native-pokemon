//React
import React, {Component, Fragment} from 'react';

//React-Router
import {withRouter} from 'react-router-dom';

//Redux
import {connect} from 'react-redux';

//React-native
import {Button} from 'react-native';

//Component
import PropTypes from 'prop-types';
import PokemonContent from '../components/PokemonContent';

//Actions
import {fetchPokemon} from '../actions/pokemon_actions';


class PokemonContentContainer extends Component {
    onPressBack = () => {
        this.props.history.goBack();
    };

    render() {
        return (
            <Fragment>
                <Button onPress={this.onPressBack} title="Retour" />
                <PokemonContent pokemon={this.props.pokemon} />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    pokemon: state.pokemonReducer.pokemon,
});

const connectedComponent = connect(mapStateToProps, {fetchPokemon})(PokemonContentContainer);
export default withRouter(connectedComponent);

PokemonContentContainer.propTypes = {
    pokemon: PropTypes.array.isRequired,
};
